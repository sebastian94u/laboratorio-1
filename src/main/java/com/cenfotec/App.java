package com.cenfotec;

public class App 
{
    public static void main( String[] args )
    {
        IncrementalNumberPrinter incrementalNumberPrinter = new IncrementalNumberPrinter();
        System.out.println(incrementalNumberPrinter.printToN(10));;
        System.out.println("------");
        DecrementalNumberPrinter decrementalNumberPrinter = new DecrementalNumberPrinter();
        System.out.println(decrementalNumberPrinter.printToN(40));;
    }
}

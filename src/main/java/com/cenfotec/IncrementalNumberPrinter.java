package com.cenfotec;

public class IncrementalNumberPrinter extends NumberPrinter {
  @Override
  public String printToN(int N) {

    String sequenceOfNumbers = "";

    if (N <= 0) {
      sequenceOfNumbers = "Cannot print numbers that are <= to 0";
    } else {
      for (int i = 1; i <= N; i++) {
        if (i > 1) {
          sequenceOfNumbers += ", ";
        }
        sequenceOfNumbers += i;
      } 
    }
    return sequenceOfNumbers;
  }
}

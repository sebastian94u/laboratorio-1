package com.cenfotec;

public abstract class NumberPrinter {
  public abstract String printToN(int N);
}

package com.cenfotec;

public class DecrementalNumberPrinter extends NumberPrinter {
  @Override
  public String printToN(int N) {

    String sequenceOfNumbers = "";

    if (N <= 0) {
      sequenceOfNumbers = "Cannot print numbers that are <= to 0";
    } else {
      for (int i = N; i >= 1; i--) {
        if (i < N) {
          sequenceOfNumbers += ", ";
        }
        sequenceOfNumbers += i;
      } 
    }

    return sequenceOfNumbers;
  }
}

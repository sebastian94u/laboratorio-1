package com.cenfotec;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class IncrementalNumberPrinterTest {

   IncrementalNumberPrinter incrementalNumberPrinter;

   @BeforeEach
   public void setUp() {
      incrementalNumberPrinter = new IncrementalNumberPrinter();
   }

   @Test
   public void testPrintToN() {
      String assertionResultToTest = "1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15";
      int n = 15;

      assertEquals(assertionResultToTest, incrementalNumberPrinter.printToN(n));
   }

   @Test
   public void testPrintToNWhenOne() {
      String assertionResultToTest = "1";
      int n = 1;

      assertEquals(assertionResultToTest, incrementalNumberPrinter.printToN(n));
   }

   @Test
   public void testPrintToNWhenZeroOrLess() {
      String assertionResultToTest = "Cannot print numbers that are <= to 0";
      int n = 0;

      assertEquals(assertionResultToTest, incrementalNumberPrinter.printToN(n));
   }
}

package com.cenfotec;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DecrementalNumberPrinterTest {

  DecrementalNumberPrinter decrementalNumberPrinter;

  @BeforeEach
  public void setUp() {
    decrementalNumberPrinter = new DecrementalNumberPrinter();
  }

  @Test
  public void testPrintToN() {
    String assertionResultToTest = "20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1";
    int n = 20;

    assertEquals(assertionResultToTest, decrementalNumberPrinter.printToN(n));
  }

  @Test
  public void testPrintToNWhenOne() {
    String assertionResultToTest = "1";
    int n = 1;

    assertEquals(assertionResultToTest, decrementalNumberPrinter.printToN(n));
  }

  @Test
  public void testPrintToNWhenZeroOrLess() {
    String assertionResultToTest = "Cannot print numbers that are <= to 0";
    int n = 0;

    assertEquals(assertionResultToTest, decrementalNumberPrinter.printToN(n));
  }
}
